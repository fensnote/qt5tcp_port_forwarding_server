﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

typedef struct svrPara
{
    QString listenPort;
    QString toSvrIp;
    QString toSvrPort;
}svrParaSt;

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    void setValue(QString lport, QString svrIp, QString toPort);

private slots:
    void on_ok_clicked();

    void on_concel_clicked();

signals:
    void sigAddItem(svrParaSt &);
    void sigModifyItem(svrParaSt &); //修改信号

private:
    Ui::Dialog *ui;
    bool m_isAdd;
};

#endif // DIALOG_H
