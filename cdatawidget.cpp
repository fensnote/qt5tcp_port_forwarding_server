#include "cdatawidget.h"
#include "ui_cdatawidget.h"
#include <QDateTime>

CDataWidget::CDataWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CDataWidget)
{
    ui->setupUi(this);
    ui->recvText->setReadOnly(true);
    ui->hex->setChecked(true);
    ui->ascii->setChecked(false);
    m_bHexMod = true;
}

CDataWidget::~CDataWidget()
{
    delete ui;
}

void CDataWidget::setName(const QString &name)
{
    ui->labTitle->setText(name);
}

//清空服务端数据显示区
void CDataWidget::on_btnClear_clicked()
{
    ui->recvText->clear();
}

void CDataWidget::on_btnSendToSvr_clicked()
{
    if ( m_bHexMod )
    {
        auto tmp = ui->toSvrData->text();
        tmp = tmp.replace(QRegExp(" "), "");
        QByteArray data = QByteArray::fromHex(tmp.toUtf8());

        emit sigSendToServer(m_id, data);
        onRecvDevData(m_id, data);
    }
    else
    {
        emit sigSendToServer(m_id, ui->toSvrData->text().toUtf8());
        onRecvDevData(m_id, ui->toSvrData->text().toUtf8());
    }
}

void CDataWidget::on_btnSendToDevice_clicked()
{
    if ( m_bHexMod )
    {
        auto tmp = ui->toDevData->text();
        tmp = tmp.replace(QRegExp(" "), "");
        QByteArray data = QByteArray::fromHex(tmp.toUtf8());

        emit sigSendToDevice(m_id, data);
        onRecvSvrData(m_id, data);
    }
    else
    {
        emit sigSendToDevice(m_id, ui->toDevData->text().toUtf8());
        onRecvSvrData(m_id, ui->toDevData->text().toUtf8());
    }
}

//选择ascii模式
void CDataWidget::on_ascii_clicked()
{
    QByteArray tmp;
    QByteArray data;
    if ( m_bHexMod )
    {
        tmp = ui->toSvrData->text().toUtf8();
        if ( !tmp.isEmpty() )
        {
            deleteSpace(tmp, tmp);
            data = QByteArray::fromHex(tmp);
            ui->toSvrData->setText(data.data());
        }

        tmp.clear();
        data.clear();
        tmp = ui->toDevData->text().toUtf8();
        if ( !tmp.isEmpty() )
        {
            deleteSpace(tmp, tmp);
            data = QByteArray::fromHex(tmp);
            ui->toDevData->setText(data.data());
        }
    }

    m_bHexMod = false;
}

//选择hex模式
void CDataWidget::on_hex_clicked()
{
    if ( !m_bHexMod )
    {
        QByteArray data = ui->toSvrData->text().toUtf8().toHex();
        insertSpace(data, data);
        ui->toSvrData->setText(data.data());

        data.clear();
        data = ui->toDevData->text().toUtf8().toHex();
        insertSpace(data, data);
        ui->toDevData->setText(data.data());

    }
    m_bHexMod = true;
}

void CDataWidget::onRecvSvrData(int id, QByteArray data)
{
    m_id = id;
    displayData(data, 1); //svr to dev
}

void CDataWidget::onRecvDevData(int id, QByteArray data)
{
    m_id = id;
    displayData(data, 2); //dev to svr
}

//flag 1 - to dev; 2 - to svr
void CDataWidget::displayData(const QByteArray &data, int flag)
{
    if (ui->recvText->toPlainText().length() > 5242880)//内容大于5M时清零
    {
        ui->recvText->clear();
    }

//    qDebug()<<"recv  data size: "<<data.size();

    QDateTime dateTime;
    QString qstrTime= dateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");

    QString color;

    if ( flag == 1 )
    {
        color = "<span style=\" color:blue;\">";
        ui->recvText->append(color+qstrTime+": id["+QString::number(m_id)+"] to device, size="+QString("%1").arg(data.size())+"</span>");
    }
    else
    {
        color = "<span style=\" color:green;\">";
        ui->recvText->append(color+qstrTime+":  id["+QString::number(m_id)+"] to server, size="+QString("%1").arg(data.size())+"</span>");
    }
    if ( m_bHexMod )
    {
        QByteArray tmp;
        insertSpace(data.toHex(), tmp);
        ui->recvText->append(color+tmp+"</span>");
    }
    else
    {
        ui->recvText->append(color+data.data()+"</span>");
    }
}

void CDataWidget::insertSpace(const QByteArray& src, QByteArray &dest)
{
    if ( src.isEmpty() )
        return;

    QByteArray tmp = src;
    dest.clear();
    for (int i = 0; i < tmp.size(); i++ )
    {
        dest.append(tmp.at(i));
        if (i % 2) dest.append(0x20);
    }
}

void CDataWidget::deleteSpace(const QByteArray& src, QByteArray &dest)
{
    if ( src.isEmpty() )
        return;

    QByteArray tmp = src;
    dest.clear();
    for (int i = 0; i < tmp.size(); i++ )
    {
        if ( tmp.at(i) == 0x20 ) //跳过空格
            continue;

        dest.append(tmp.at(i));
    }
}
