﻿#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>


Dialog::Dialog( QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowTitle("转发任务");
    //ui->listenPort->setStyleSheet("border :1px ;background : (0x00,0xff,0x00,0x00)");
    m_isAdd = true;
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_ok_clicked()
{
    if ( ui->listenPort->text().isEmpty() || \
         ui->forwardIp->text().isEmpty()  ||\
         ui->forwordPort->text().isEmpty() )
    {
        QMessageBox::warning(this, tr("forwardSvr warning"),
                                       tr("please check input\n"),
                                       QMessageBox::Ok );

        return;
    }

    svrParaSt svrPara;
    svrPara.listenPort = ui->listenPort->text();
    svrPara.toSvrIp    = ui->forwardIp->text();
    svrPara.toSvrPort  = ui->forwordPort->text();

    if ( m_isAdd )
        emit sigAddItem(svrPara);
    else
        emit sigModifyItem(svrPara);

    this->close();
}

void Dialog::on_concel_clicked()
{
    this->close();
}

void Dialog::setValue(QString lport, QString svrIp, QString toPort)
{
    m_isAdd = false; //有初始化数据，代表是修改；
    ui->listenPort->setText(lport);
    ui->forwardIp->setText(svrIp);
    ui->forwordPort->setText(toPort);
}
