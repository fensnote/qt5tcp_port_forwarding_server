#ifndef CTASKSERVICE_H
#define CTASKSERVICE_H

#include <QObject>
#include <QMap>
#include <QTimer>
#include <QtNetwork>
#include <QStandardItemModel>
#include "ctaskproc.h"

typedef struct forwardPara
{
    int listenPort;
    QString toIp;
    int toPort;
    QStandardItem* itemProject;

}forwardParaSt;


class CTaskService : public QObject
{
    Q_OBJECT
public:
    explicit CTaskService(forwardParaSt* &para, QObject *parent = 0);
    ~CTaskService();

    void startService();
    void stopService();
    void disMsg(QString str);
    void setSelectId(int id);
    void stopPrintData();

private:    
    void stopAllClient();

signals:
    void sendErrorMsg(QString);
    void sendLocalClientData(int id, QByteArray &data);
    void sendSvrClientData(int id, QByteArray &data);
    void removeClient(int ,int);
    void addClient(int ,int);

public slots:
    void onSendToServer(int id, QByteArray data); //发给svr的数据
    void onSendToDevice(int id, QByteArray data); //发给dev的数据

    void acceptConnect();   //接收客户端连接
    void onProcTaskEnd(int id);
    void onProcTaskStartOk(int id);
    void onRecvLocalClientData(int id, QByteArray &data);
    void onRecvSvrClientData(int id, QByteArray &data);
    void checkTask();

private:
    forwardParaSt* m_forwardPara;
    QTcpServer *m_TcpServer;

    QTimer *m_checkTimer;

    int m_clientId;
    bool m_printDataFlag; //打印数据标志;
    int  m_printDataId;   //当前选中输出数据的任务id
    QMap<int, CTaskProc*> m_taskMap;//使用行号作为id任务号
};

#endif // CTASKSERVICE_H
