﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QStandardItemModel>
#include "ctaskservice.h"
#include "dialog.h"
#include "cdatawidget.h"

namespace Ui {
class MainWindow;
}
#define config_file "forwardSvr.json"



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void initForwardSvrList();
    forwardParaSt * creatForwardSvr(int listenPort, QString toIp, int toPort);

private:
    QModelIndex getTopParent(const QModelIndex &itemIndex);
    void selectPrintData(QString name, int id);
    void removeItem(int id,QStandardItem *&svrItem);
    void addForwardSvrToConfig(int listenPort, QString toSvrIp, int toSvrPort);
    void delForwardSvrToConfig(int listenPort);

public slots:
    void recvAddSvrPara(svrParaSt &SvrPara);
    void recvModifySvrPara(svrParaSt &SvrPara);
    void removeClientItem(int port, int id);
    void addClientItem(int port, int id);

private slots:
    void on_forwardListView_clicked(const QModelIndex &index);

    void on_actionStart_triggered();

    void on_actionStop_triggered();

    void on_actionAdd_triggered();

    void on_actionDel_triggered();

    void on_actionAbout_triggered();

    void on_actionmdify_triggered();


private:

    Ui::MainWindow *ui;
    QStandardItemModel * model;
    QMap<int,forwardParaSt*> m_forwardList;
    QMap<QString,CTaskService*> m_TaskServiceList;

    QString nowPrintDataSerivice; //当前输出数据的服务组备份
    QModelIndex m_nowSelectSvrRow;        //当前选中的转发服务行号
    int m_nowSelectListenPort;        //当前选中的转发服务监听端口号
    QString m_nowSelectItem;          //当前选中的转发服务名称
    Dialog *m_setSvr;

    QMap<QString, CDataWidget*> m_devWidgetMap; //每个连接对应一个日志页面
};

#endif // MAINWINDOW_H
