#ifndef CDATAWIDGET_H
#define CDATAWIDGET_H

#include <QWidget>

namespace Ui {
class CDataWidget;
}

class CDataWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CDataWidget(QWidget *parent = 0);
    ~CDataWidget();
    void setName(const QString &name);

private:
    void displayData(const QByteArray &data, int flag);
    void insertSpace(const QByteArray& src, QByteArray &dest);
    void deleteSpace(const QByteArray& src, QByteArray &dest);

public slots:
    void onRecvSvrData(int id, QByteArray data);
    void onRecvDevData(int id, QByteArray data);

signals:
    void sigSendToServer(int id, QByteArray data);
    void sigSendToDevice(int id, QByteArray data);

private slots:
    void on_btnClear_clicked();

    void on_btnSendToSvr_clicked();

    void on_btnSendToDevice_clicked();

    void on_hex_clicked();
    void on_ascii_clicked();


private:
    Ui::CDataWidget *ui;
    bool m_bHexMod; //true -- hex mode； false-- ascii mode
    int m_id; //用于区分socket
};

#endif // CDATAWIDGET_H
